/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.serviceDependency;
import junit.framework.TestCase;

import org.osgi.service.log.LogService;

public class BundleContextInjectionTest extends TestCase {
    private volatile LogService m_log;

    public void testLogServiceIsInjectedOk() {
        assertNotNull("Log service not injected?!", m_log);
    }
    
    @Override
    protected void setUp() throws Exception {
        configure(this)
        .add(serviceDependency(LogService.class))
        .apply();
    }
    
    @Override
    protected void tearDown() throws Exception {
        cleanUp(this);
    }
}
