/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.mongo;

import static org.amdatu.testing.configurator.TestUtils.deleteConfiguration;
import static org.amdatu.testing.configurator.TestUtils.getFactoryConfiguration;
import static org.amdatu.testing.configurator.TestUtils.getService;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.testing.configurator.ConfigurationStep;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * A configuration step that sets up a temporary mongo database and drops it on cleanup. 
 */
public class MongoConfigurationStep implements ConfigurationStep {

	private String dbName;
	private String mongoInstancePid;
	
	@Override
	public void apply(Object testCase) {
		Properties props = new Properties();
		dbName = "test-" + System.currentTimeMillis() + "-" + testCase.getClass().getSimpleName();
		props.put("dbName", dbName);
		
		Configuration configuration = getFactoryConfiguration("org.amdatu.mongo");
		try {
			configuration.update(props);
			this.mongoInstancePid = configuration.getPid();
		} catch (IOException e) {
			throw new RuntimeException("Could not configure Mongo", e);
		}
	}
	
	public void cleanUp(Object testCase) {
		MongoDBService mongoDbService = getService(MongoDBService.class);
		mongoDbService.getDB().dropDatabase();
		
		// Construct a ServiceTracker that waits for the specific MongoDBService to go down. Only then the clean up is concluded.
		// We use the dbName service property for that, since it is added by the amdatu.mongo implementation and uniquely identifies
		// the database we are cleaning up.
		final CountDownLatch countDownLatch = new CountDownLatch(1);
		
		final BundleContext context = FrameworkUtil.getBundle(MongoConfigurationStep.class).getBundleContext();
		String classFilter = "(" + Constants.OBJECTCLASS + "="
				+ MongoDBService.class.getName() + ")";
		String dbFilter = "(dbName=" + dbName + ")";  
		String filterString = "(&" + classFilter + dbFilter + ")";
		ServiceTracker serviceTracker = null;
		try {
			serviceTracker = new ServiceTracker(context, context.createFilter(filterString), new ServiceTrackerCustomizer() {
				
				@Override
				public void removedService(ServiceReference ref, Object instance) {
					countDownLatch.countDown();
				}
				
				@Override
				public void modifiedService(ServiceReference ref, Object instance) {
					
				}
				
				@Override
				public Object addingService(ServiceReference ref) {
					return context.getService(ref);
				}
				
			});
			serviceTracker.open();
			
			// This is asynchronous, so we cannot be sure the MongoDBService instance is gone
			deleteConfiguration(mongoInstancePid);
			
			// Therefore we wait until our servicetracker detects the service going away.
			boolean success = countDownLatch.await(2, TimeUnit.SECONDS);
			if(!success) {
				throw new RuntimeException("Could not remove MongoDBService with pid " + mongoInstancePid);
			}
		} catch (InvalidSyntaxException | InterruptedException e) {
			throw new RuntimeException("Problem doing cleanup for MongoDBService with pid " + mongoInstancePid, e);
		} finally {
			if(serviceTracker != null) {
				serviceTracker.close();
			}
		}
		
	}
}
